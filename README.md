Das JavaScript zur Anpassung des Verhaltens von PAUL und RemUI ist einzufügen unter:

Website-Administration > Darstellung > Zusätzliches HTML > Vor dem Schließen von BODY

Direkter Link: https://paul.zhdk.ch/admin/settings.php?section=additionalhtml

Nach Änderungen am Code den Cache löschen: https://paul.zhdk.ch/admin/purgecaches.php
